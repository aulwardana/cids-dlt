package main

import (
	"bytes"
	"encoding/gob"
	"log"

	"github.com/dgraph-io/badger"
	"github.com/sjwhitworth/golearn/base"
)

//Create DB file for saving database
const dbPathPoW = "./tmp/blocksPow"

//const dbPathPoAD = "./tmp/blocksPoAD"

/*
The block for PoW algorithm consist of 4 different value
1. Hash: this value is used for currect value of the block
2. Data: this value is used for save any data to block
3. PrevHash: this value is the previous hash value from previous block
4. Nonce: https://en.wikipedia.org/wiki/Cryptographic_nonce
*/
type BlockPoW struct {
	Hash     []byte
	Data     []byte
	PrevHash []byte
	Nonce    int
}

/*
Data from PoAD consist of task for anomaly detection.
There are 3 main value from this task. Here is the list:
1. Algorithm: choose the algorithm for training dataset
2. Accuracy: target accuracy must achieve
3. Time: target of time limit
4. Dataset: type of dataset that will be used
*/
type DataPoAD struct {
	Algorithm string
	Accuracy  float64
	Time      int
	Dataset   string
}

/*
The block for PoAD algorithm consist of 4 different value
1. Hash: this value is used for currect value of the block
2. Data: this value is used for save any data to block
3. PrevHash: this value is the previous hash value from previous block
4. Model: Machine learning model result
5. Datatest: Test data to validate model
6. Accuracy: Accuracy result from the model
7. Result: anomaly detection result (true/false)
*/
type BlockPoAD struct {
	Hash     []byte
	Data     *DataPoAD
	PrevHash []byte
	Model    base.FixedDataGrid
	Datatest base.FixedDataGrid
	Accuracy float64
	Result   bool
}

// One single blockchain struct of PoW consist of some block struct
type BlockChainPoW struct {
	//blocks   []*BlockPoW
	LastHash []byte
	Database *badger.DB
}

type BlockChainIteratorPoW struct {
	CurrentHash []byte
	Database    *badger.DB
}

// One single blockchain struct of PoAD consist of some block struct
type BlockChainPoAD struct {
	blocks []*BlockPoAD
}

/*=========================== Global Function ===========================*/

//error handling function
func Handle(err error) {
	if err != nil {
		log.Panic(err)
	}
}

/*=========================== PoW Algorithm ===========================*/

/*
This function is creating a block by input data and previous hash from previous block.
After that, the block will have structure value like "BlockPoW struct"
*/
func CreateBlockPoW(data string, prevHash []byte) *BlockPoW {
	block := &BlockPoW{[]byte{}, []byte(data), prevHash, 0}

	//Run PoW algorithm
	poad := NewProofOfWork(block)
	nonce, hash := poad.RunPoW()

	block.Hash = hash[:]
	block.Nonce = nonce
	return block
}

//This function is inputing data to a block.
func (chain *BlockChainPoW) AddBlockPoW(data string) {
	var lastHash []byte

	err := chain.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte("lh"))
		Handle(err)
		err = item.Value(func(val []byte) error {
			lastHash = val
			return nil
		})
		Handle(err)
		return err
	})
	Handle(err)

	newBlock := CreateBlockPoW(data, lastHash)

	err = chain.Database.Update(func(transaction *badger.Txn) error {
		err := transaction.Set(newBlock.Hash, newBlock.Serialize())
		Handle(err)
		err = transaction.Set([]byte("lh"), newBlock.Hash)

		chain.LastHash = newBlock.Hash
		return err
	})
	Handle(err)

	/*prevBlock := chain.blocks[len(chain.blocks)-1]
	new := CreateBlockPoW(data, prevBlock.Hash)
	chain.blocks = append(chain.blocks, new)*/
}

//This function is to create the first block.
func GenesisPoW() *BlockPoW {
	return CreateBlockPoW("Genesis", []byte{})
}

//Create initialization for start first block
func InitBlockchainPoW() *BlockChainPoW {
	var lastHash []byte

	opts := badger.DefaultOptions(dbPathPoW)

	db, err := badger.Open(opts)
	Handle(err)

	err = db.Update(func(txn *badger.Txn) error {
		if _, err := txn.Get([]byte("lh")); err == badger.ErrKeyNotFound {
			genesis := GenesisPoW()
			err = txn.Set(genesis.Hash, genesis.Serialize())
			Handle(err)
			err = txn.Set([]byte("lh"), genesis.Hash)

			lastHash = genesis.Hash

			return err
		} else {
			item, err := txn.Get([]byte("lh"))
			Handle(err)
			err = item.Value(func(val []byte) error {
				lastHash = val
				return nil
			})
			Handle(err)
			return err
		}
	})
	Handle(err)

	blockchain := BlockChainPoW{lastHash, db}
	return &blockchain

	//return &BlockChainPoW{[]*BlockPoW{GenesisPoW()}}
}

//serialize data function
func (b *BlockPoW) Serialize() []byte {
	var res bytes.Buffer
	encoder := gob.NewEncoder(&res)

	err := encoder.Encode(b)

	Handle(err)

	return res.Bytes()
}

//deserialize data function
func Deserialize(data []byte) *BlockPoW {
	var block BlockPoW

	decoder := gob.NewDecoder(bytes.NewReader(data))

	err := decoder.Decode(&block)

	Handle(err)

	return &block
}

func (chain *BlockChainPoW) IteratorPoW() *BlockChainIteratorPoW {
	iterator := BlockChainIteratorPoW{chain.LastHash, chain.Database}

	return &iterator
}

//Looking block into database using iterator
func (iterator *BlockChainIteratorPoW) NextPoW() *BlockPoW {
	var block *BlockPoW

	err := iterator.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get(iterator.CurrentHash)
		Handle(err)

		err = item.Value(func(val []byte) error {
			block = Deserialize(val)
			return nil
		})
		Handle(err)
		return err
	})
	Handle(err)

	iterator.CurrentHash = block.PrevHash

	return block
}

/*=========================== PoAD Algorithm ===========================*/

/*
This function is creating a block by input data and previous hash from previous block.
After that, the block will have structure value like "BlockPoAD struct"
*/
func CreateBlockPoAD(data *DataPoAD, prevHash []byte) *BlockPoAD {
	block := &BlockPoAD{[]byte{}, data, prevHash, nil, nil, 0, false}

	//Run PoAD algorithm
	poad := NewProofOfAnomaly(block)
	model, test, hash, acc, result := poad.RunPoAD()

	block.Hash = hash[:]
	block.Model = model
	block.Datatest = test
	block.Accuracy = acc
	block.Result = result
	return block
}

//This function is inputing data to a block.
func (chain *BlockChainPoAD) AddBlockPoAD(data *DataPoAD) {
	prevBlock := chain.blocks[len(chain.blocks)-1]
	new := CreateBlockPoAD(data, prevBlock.Hash)
	chain.blocks = append(chain.blocks, new)
}

//This function is to create the first block.
func GenesisPoAD() *BlockPoAD {
	genesis := &DataPoAD{"Genesis", 0, 0, "Genesis"}

	return CreateBlockPoAD(genesis, []byte{})
}

//Create initialization for start first block
func InitBlockchainPoAD() *BlockChainPoAD {
	return &BlockChainPoAD{[]*BlockPoAD{GenesisPoAD()}}
}
