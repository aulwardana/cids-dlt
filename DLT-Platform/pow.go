package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"log"
	"math"
	"math/big"
)

/*
Requirement: The first few bytes must contain 0's.

The step-by-step of PoW looks something like this:
1. Take the data from the block
2. Create a nonce (https://en.wikipedia.org/wiki/Cryptographic_nonce)
3. Create a hash of the data from the block + the nonce
4. Check the proceeding hash to see if it meets the requirements (the first few bytes must be 0's).
*/

//Value for determine level of difficult of the task. Real implementation this value is growing depend on the number of miner and computation power of the  miner.
const Difficulty = 12

type ProofOfWork struct {
	Block  *BlockPoW //Block that will be process
	Target *big.Int  //Number that represent the requirement
}

//Step 1

func NewProofOfWork(b *BlockPoW) *ProofOfWork {
	target := big.NewInt(1) //generate target

	/*
		This library includes various Locality Sensitive Hashing (LSH) algorithms for the approximate nearest neighbour search problem in L2 metric space.
		Lsh function is implementation from this paper: https://www.cs.princeton.edu/courses/archive/spr05/cos598E/bib/p253-datar.pdf
	*/
	target.Lsh(target, uint(256-Difficulty)) //256 is size of hash. 256 value will substract by difficulty and process by LSH funtion to achieve the target

	pow := &ProofOfWork{b, target} //Put block and target into one struct of PoW

	return pow
}

//Hex value converter (int64 to byte)
func ToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num)
	if err != nil {
		log.Panic(err)
	}
	return buff.Bytes()
}

//Step 2

/*
Join pervious hash of block, data, nonce, and difficulty value to create the hash.
This hash will used for hashing of current block
*/
func (pow *ProofOfWork) InitNonce(nonce int) []byte {
	data := bytes.Join(
		[][]byte{
			pow.Block.PrevHash,
			pow.Block.Data,
			ToHex(int64(nonce)),
			ToHex(int64(Difficulty)),
		},
		[]byte{},
	)
	return data
}

//Step 3

func (pow *ProofOfWork) RunPoW() (int, []byte) {
	var intHash big.Int
	var hash [32]byte

	nonce := 0 //start nonce with 0

	// This is essentially an infinite loop due to how large of MaxInt64.
	for nonce < math.MaxInt64 {
		data := pow.InitNonce(nonce) //Add nonce value to create data using InitNonce Function
		hash = sha256.Sum256(data)   //After data created, then hash the data using SHA 236 method

		fmt.Printf("\r%x", hash)
		intHash.SetBytes(hash[:]) //set intHash value with hash value from the data

		//Compare
		if intHash.Cmp(pow.Target) == -1 { //compare intHash value with target value
			break //if the target is achieve, then this loop will end
		} else {
			nonce++ //If not below zero, increase nonce value. It means the target not achieve
		}

	}
	fmt.Println()

	return nonce, hash[:] //the combination and hash value that achieve the target is found
}

//Step 4

//This function is to validate the work
func (pow *ProofOfWork) ValidatePoW() bool {
	var intHash big.Int

	data := pow.InitNonce(pow.Block.Nonce) //take nonce from the struct to create data

	hash := sha256.Sum256(data) //hash the data using SHA 256
	intHash.SetBytes(hash[:])   //set hash to big int in intHash value

	/*
		If the intHash is achieve the target value, then it's return true.
		When it not achieve, then return false
	*/
	return intHash.Cmp(pow.Target) == -1
}
