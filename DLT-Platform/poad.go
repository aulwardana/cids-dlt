package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"time"

	"github.com/sjwhitworth/golearn/base"
	"github.com/sjwhitworth/golearn/evaluation"
	"github.com/sjwhitworth/golearn/knn"
	"github.com/sjwhitworth/golearn/trees"
)

/*
Requirement: the learning model must achieve accuracy and time limit.

The step-by-step of PoAD looks something like this:
1. Take the data from the block
2. Get learning task specification (time limit and accuracy)
3. Create a hash of the data from the block
4. Check the model to see if it meets the requirements (accuracy and time limit).
*/

type ProofOfAnomaly struct {
	Block     *BlockPoAD //Block that will be process
	Algorithm string     //Algorithm used for task
	Accuracy  float64    //Accuracy target
	Time      int        //Time limit target
	Dataset   string     //Dataset from the task
}

//Extract data from global struct to local struct
func NewProofOfAnomaly(b *BlockPoAD) *ProofOfAnomaly {
	algorithm := b.Data.Algorithm
	accuracy := b.Data.Accuracy
	time := b.Data.Time
	dataset := b.Data.Dataset

	poad := &ProofOfAnomaly{b, algorithm, accuracy, time, dataset}

	return poad
}

/*
Join pervious hash of block, data, model, accuracy, and result value to create the hash.
This hash will used for hashing of current block
*/
func (poad *ProofOfAnomaly) JoinBlock(dt *DataPoAD, mdl *base.FixedDataGrid, rst bool) []byte {
	var bin_buf_dt bytes.Buffer
	var bin_buf_mdl bytes.Buffer
	var bin_buf_rst bytes.Buffer

	binary.Write(&bin_buf_dt, binary.BigEndian, dt)
	binary.Write(&bin_buf_mdl, binary.BigEndian, mdl)
	binary.Write(&bin_buf_rst, binary.BigEndian, rst)

	data := bytes.Join(
		[][]byte{
			poad.Block.PrevHash,
			bin_buf_dt.Bytes(),
			bin_buf_mdl.Bytes(),
			bin_buf_rst.Bytes(),
		},
		[]byte{},
	)
	return data
}

/*============================ Start Machine Learning Task ============================*/

/*
This area is free to modify, you can add mode machine learning algorithm in here.
For fast implementation, the machine learning algorithm based on this library: https://github.com/sjwhitworth/golearn/
Support machine learning: KNN, Decission Tree
*/

//KNN Algorithm processing
func KNNAlg(dataset string) (base.FixedDataGrid, base.FixedDataGrid, float64) {
	var model base.FixedDataGrid
	var test base.FixedDataGrid
	var acc float64

	//Get dataset and processed to raw data. Dataset must CSV format.
	rawData, err := base.ParseCSVToInstances(dataset, true)
	if err != nil {
		panic(err)
	}

	//Initialises a new KNN classifier
	cls := knn.NewKnnClassifier("euclidean", "linear", 2)

	//Do a training-test split
	trainData, testData := base.InstancesTrainTestSplit(rawData, 0.50)
	cls.Fit(trainData)

	test = testData

	//Calculates the Euclidean distance and returns the most popular label
	predictions, err := cls.Predict(testData)
	if err != nil {
		panic(err)
	}

	model = predictions

	//Evaluate
	confusionMat, err := evaluation.GetConfusionMatrix(testData, predictions)
	if err != nil {
		panic(fmt.Sprintf("Unable to get confusion matrix: %s", err.Error()))
	}

	//Get accuracy
	acc = evaluation.GetAccuracy(confusionMat) * 100

	return model, test, acc
}

//Decission Tree Algorithm processing
func D3Alg(dataset string) (base.FixedDataGrid, base.FixedDataGrid, float64) {
	var model base.FixedDataGrid
	var test base.FixedDataGrid
	var acc float64
	var tree base.Classifier

	//Get dataset and processed to raw data. Dataset must CSV format.
	rawData, err := base.ParseCSVToInstances(dataset, true)
	if err != nil {
		panic(err)
	}

	// Create a 60-40 training-test split
	trainData, testData := base.InstancesTrainTestSplit(rawData, 0.60)

	test = testData

	// First up, use ID3
	tree = trees.NewID3DecisionTree(0.6)
	// (Parameter controls train-prune split.)

	// Train the ID3 tree
	tree.Fit(trainData)

	// Generate predictions
	predictions, err := tree.Predict(testData)
	if err != nil {
		panic(err)
	}

	model = predictions

	// Evaluate
	cf, err := evaluation.GetConfusionMatrix(testData, predictions)
	if err != nil {
		panic(fmt.Sprintf("Unable to get confusion matrix: %s", err.Error()))
	}

	//Get accuracy
	acc = evaluation.GetAccuracy(cf) * 100

	return model, test, acc
}

/*============================ End Machine Learning Task ============================*/

func (poad *ProofOfAnomaly) RunPoAD() (base.FixedDataGrid, base.FixedDataGrid, []byte, float64, bool) {
	var hash [32]byte
	var model base.FixedDataGrid
	var test base.FixedDataGrid
	var acc float64
	var result bool
	var arrayAcc []float64
	var arrayMdl []base.FixedDataGrid
	var arrayTest []base.FixedDataGrid

	if poad.Algorithm == "Genesis" && poad.Dataset == "Genesis" { //Checking if genesis block
		model = nil
		test = nil
		result = false
		hash = sha256.Sum256(poad.JoinBlock(poad.Block.Data, &model, result))
	} else {
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		done := make(chan bool)
		go func() {
			//Get learning time limit from task and process
			time.Sleep(time.Duration(poad.Time) * time.Second)
			done <- true
		}()
		for {
			select {
			case <-done:
				return model, test, hash[:], acc, result
			case <-ticker.C:
				//When time start, the algorithm will process here
				if poad.Algorithm == "KNN" {
					model, test, acc = KNNAlg(poad.Dataset) //Process with KNN
				} else if poad.Algorithm == "D3" {
					model, test, acc = D3Alg(poad.Dataset) //Process with D3
				} else {
					model = nil
					test = nil
					acc = 0
				}

				arrayAcc = append(arrayAcc, acc)    //Get all learning accuracy result
				arrayMdl = append(arrayMdl, model)  //Get all learning model result
				arrayTest = append(arrayTest, test) //Get all learning datatest result

				//Sort best accuracy for finding best model
				for j := 1; j < len(arrayAcc); j++ {
					if arrayAcc[0] < arrayAcc[j] {
						arrayAcc[0] = arrayAcc[j]
						arrayMdl[0] = arrayMdl[j]
						arrayTest[0] = arrayTest[j]
					}
				}

				//Check if accuracy is achieve the target and apply best accuracy model
				if arrayAcc[0] >= poad.Accuracy {
					model = arrayMdl[0]
					acc = arrayAcc[0]
					test = arrayTest[0]
					result = true
					hash = sha256.Sum256(poad.JoinBlock(poad.Block.Data, &model, result))
					break
				} else {
					model = arrayMdl[0]
					acc = arrayAcc[0]
					test = arrayTest[0]
					result = false
					hash = sha256.Sum256(poad.JoinBlock(poad.Block.Data, &model, result))
				}
			}
		}
	}

	return model, test, hash[:], acc, result
}

//Evaluate the block
func (poad *ProofOfAnomaly) ValidatePoAD() bool {

	if poad.Block.Data.Dataset == "Genesis" { //check if genesis block
		return false
	} else {
		//Evaluation based on stat test and model
		confusionMat, err := evaluation.GetConfusionMatrix(poad.Block.Datatest, poad.Block.Model)
		if err != nil {
			panic(fmt.Sprintf("Unable to get confusion matrix: %s", err.Error()))
		}

		//Get Accuracy
		acc := evaluation.GetAccuracy(confusionMat) * 100

		//If accuracy achieve target
		if acc >= poad.Accuracy {
			return true
		}
	}

	return false
}
