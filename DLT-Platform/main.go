package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strconv"
)

// This blockchain simulation build based on blockchain white paper https://bitcoin.org/bitcoin.pdf

type CommandLine struct {
	blockchain *BlockChainPoW
}

func (cli *CommandLine) printUsage() {
	fmt.Println("Usage: ")
	fmt.Println(" add -block <BLOCK_DATA> - add a block to the chain")
	fmt.Println(" print - prints the blocks in the chain")
}

func (cli *CommandLine) validateArgs() {
	if len(os.Args) < 2 {
		cli.printUsage()
		//go exit will exit the application by shutting down the goroutine
		// if you were to use os.exit you might corrupt the data
		runtime.Goexit()
	}
}

func (cli *CommandLine) addBlock(data string) {
	cli.blockchain.AddBlockPoW(data)
	fmt.Println("Added Block!")
}

func (cli *CommandLine) printChain() {
	iterator := cli.blockchain.IteratorPoW()

	for {
		block := iterator.NextPoW()
		fmt.Printf("Previous hash: %x\n", block.PrevHash)
		fmt.Printf("data: %s\n", block.Data)
		fmt.Printf("hash: %x\n", block.Hash)
		pow := NewProofOfWork(block)
		fmt.Printf("Pow: %s\n", strconv.FormatBool(pow.ValidatePoW()))
		fmt.Println()
		// This works because the Genesis block has no PrevHash to point to.
		if len(block.PrevHash) == 0 {
			break
		}
	}
}

func (cli *CommandLine) run() {
	cli.validateArgs()

	addBlockCmd := flag.NewFlagSet("add", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("print", flag.ExitOnError)
	addBlockData := addBlockCmd.String("block", "", "Block data")

	switch os.Args[1] {
	case "add":
		err := addBlockCmd.Parse(os.Args[2:])
		Handle(err)

	case "print":
		err := printChainCmd.Parse(os.Args[2:])
		Handle(err)

	default:
		cli.printUsage()
		runtime.Goexit()
	}
	// Parsed() will return true if the object it was used on has been called
	if addBlockCmd.Parsed() {
		if *addBlockData == "" {
			addBlockCmd.Usage()
			runtime.Goexit()
		}
		cli.addBlock(*addBlockData)
	}
	if printChainCmd.Parsed() {
		cli.printChain()
	}
}

func main() {

	defer os.Exit(0)

	chain := InitBlockchainPoW()
	defer chain.Database.Close()

	cli := CommandLine{chain}

	cli.run()

	/*
		//create initialization for genesis block
		chainPoW := InitBlockchainPoW()

		//Add some data to block
		chainPoW.AddBlockPoW("First block after genesis")
		chainPoW.AddBlockPoW("Second block after genesis")
		chainPoW.AddBlockPoW("Third block after genesis")

		for _, blockpow := range chainPoW.blocks {
			//Print all data with current hash and previous hash infromation
			fmt.Printf("Previous hash: %x\n", blockpow.PrevHash)
			fmt.Printf("data: %s\n", blockpow.Data)
			fmt.Printf("hash: %x\n", blockpow.Hash)

			//PoW validation
			pow := NewProofOfWork(blockpow)
			fmt.Printf("Pow: %s\n", strconv.FormatBool(pow.ValidatePoW()))
			fmt.Println()
		}

		//create initialization for genesis block
		chainPoAD := InitBlockchainPoAD()

		/*
			Choose the algorithm
			1. K-Nearest Neighbors (code: KNN)
			2. Decision Tree (code: D3)
	*/
	/*
		//Add some data to block
		block1 := &DataPoAD{"KNN", 90, 10, "dataset/iris.csv"}
		block2 := &DataPoAD{"D3", 90, 10, "dataset/iris.csv"}
		//block3 := &DataPoAD{90, 50, "dataset/iris.csv"}

		chainPoAD.AddBlockPoAD(block1)
		chainPoAD.AddBlockPoAD(block2)
		//chainPoAD.AddBlockPoAD(block3)

		for _, blockpoad := range chainPoAD.blocks {
			//Print all data with current hash and previous hash infromation
			fmt.Printf("Previous hash: %x\n", blockpoad.PrevHash)
			fmt.Printf("%+v\n", blockpoad.Data)
			fmt.Printf("hash: %x\n", blockpoad.Hash)
			fmt.Printf("%f\n", blockpoad.Accuracy)
			fmt.Printf("%+v\n", blockpoad.Model)
			fmt.Printf("result: %t\n", blockpoad.Result)

			//PoAD validation
			poad := NewProofOfAnomaly(blockpoad)
			fmt.Printf("PoAD: %s\n", strconv.FormatBool(poad.ValidatePoAD()))
			fmt.Println()
		}
	*/
}
