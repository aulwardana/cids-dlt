# Python CICFlowMeter

> This project is cloned from [cicflowmeter](https://github.com/datthinh1801/cicflowmeter). I used this tool in my PhD disertation for experiment. This tool is tested by me on ubuntu server (do not install minimal version of ubuntu server).  


### Installation
```
clone this repository using git
cd cicflowmeter
python3 setup.py install
```

### Usage
```
usage: cicflowmeter [-h] (-i INPUT_INTERFACE | -f INPUT_FILE) [-c] [-u URL_MODEL] output

positional arguments:
  output                output file name (in flow mode) or directory (in sequence mode)

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_INTERFACE    capture online data from INPUT_INTERFACE
  -f INPUT_FILE         capture offline data from INPUT_FILE
  -c, --csv, --flow     output flows as csv
```

Convert pcap file to flow csv:

```
mkdir -p /home/capture/
cd /home/capture/
cicflowmeter -f example.pcap -c flows.csv
```

Sniff packets real-time from interface to flow csv: (**need root permission**)

```
mkdir -p /home/capture/
cd /home/capture/
cicflowmeter -i eth0 -c flows.csv
```

- Reference: https://www.unb.ca/cic/research/applications.html#CICFlowMeter
