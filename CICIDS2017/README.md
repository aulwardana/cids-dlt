# CICIDS2017

CICIDS2017 dataset contains benign and the most up-to-date common attacks, which resembles the true real-world data (PCAPs). It also includes the results of the network traffic analysis using CICFlowMeter with labeled flows based on the time stamp, source, and destination IPs, source and destination ports, protocols and attack (CSV files).   


### Dataset Download

Download here: https://www.unb.ca/cic/datasets/ids-2017.html

### Tool for Preprocessing CICIDS2017 Dataset

Please see on preprocess-tool folder 